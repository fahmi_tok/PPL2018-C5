import axios from 'axios';
import qs from 'query-string';

export const state = () => ({
  list: [],
  active: {},
  meta: {},
  error: null,
  offset: 0,
  limit: 6,
  currentPage: 1,
  hitCount: 0,
  uploadID: 0,
  keyword: '',
  filters: [],
  prevPage: ''
})

export const mutations = {
  SET_PRODUCTS (state, products) {
    state.list = products
  },

  SET_PRODUCT (state, product) {
    state.active = product
  },

  SET_METADATA (state, meta) {
    state.meta = meta
  },

  SET_FAILURE (state, error) {
    state.error = error
  },

  SET_OFFSET (state, offset) {
    state.offset = offset
  },

  SET_LIMIT (state, limit) {
    state.limit = limit
  },

  SET_PAGE (state, page) {
    state.currentPage = page
  },

  SET_UPLOAD_ID (state, uploadID) {
    state.uploadID = uploadID
  },

  SET_HITCOUNT (state, hitCount) {
    state.hitCount = hitCount
  },

  SET_KEYWORD (state, keyword) {
    state.keyword = keyword
  },

  SET_FILTERS (state, filters) {
    state.filters = filters
  },

  SET_PREV_PAGE (state, prevPage) {
    state.prevPage = prevPage
  }
}

export const actions = {

  BULK_UPLOAD_PRODUCT ({ commit }, csvFile) {
    const formData = new FormData();
    formData.append('file', csvFile, csvFile.name);

    return axios.post(process.env.BASE_API_URL + '/product', formData)
    .then((response) => {
      commit('SET_METADATA', response.data.Meta)
      commit('SET_UPLOAD_ID', response.data.Body)
    })
    .catch((error) => commit('SET_FAILURE', error));
  },

  FETCH_ALL_PRODUCT ({ commit, state }) {
    return axios.get(process.env.BASE_API_URL + '/product', {
      params: {keyword: state.keyword, filter: state.filters, limit: state.limit, offset: state.offset}, 
      paramsSerializer: function(params) {
        return qs.stringify(params)
      },
    })
    .then((response) => {
      commit('SET_HITCOUNT', response.data.Body.HitCount)
      commit('SET_PRODUCTS', response.data.Body.Products)
      commit('SET_METADATA', response.data.Meta)
    })
    .catch((error) => commit('SET_FAILURE', error));
  },


  FETCH_PRODUCTS_BY_UPLOAD_ID ({ commit, state }) {
    return axios.get(process.env.BASE_API_URL + '/preview/' + state.uploadID.toString(), {
      params: {keyword: state.keyword, filter: state.filters, limit: state.limit, offset: state.offset}, 
      paramsSerializer: function(params) {
        return qs.stringify(params)
      },
    })
    .then((response) => {
      commit('SET_HITCOUNT', response.data.Body.HitCount)
      commit('SET_PRODUCTS', response.data.Body.Products)
      commit('SET_METADATA', response.data.Meta)
    })
    .catch((error) => commit('SET_FAILURE', error));
  },

  SET_PAGINATION ({ commit, state }, { offset, page }) {
    commit('SET_OFFSET', offset)
    commit('SET_PAGE', page)
  },

  FETCH_PRODUCT_BY_ID ({ commit }, id) {
    return axios.get(process.env.BASE_API_URL + '/product/' + id.toString())
    .then((response) => {
      commit('SET_PRODUCT', response.data.Body)
      commit('SET_METADATA', response.data.Meta)
    })
    .catch((error) => commit('SET_FAILURE', error));
  },

  SET_SEARCH_KEYWORD ({ commit, state }, { keyword }) {
    commit('SET_KEYWORD', keyword)
  },

  SET_DISPLAY_FILTERS ({ commit, state }, { filters }) {
    commit('SET_FILTERS', filters)
  },

  SET_PREVIOUS_PAGE ({ commit, state }, { prevPage }) {
    commit('SET_PREV_PAGE', prevPage)
  },

}
