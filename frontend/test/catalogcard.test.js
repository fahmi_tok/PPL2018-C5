import Vue from 'vue';
import { shallow } from 'avoriaz';
import { mount } from 'avoriaz';
import CatalogCard from '../components/CatalogCard';
import Button from '../components/Button';

describe('Components ', () => {
  let cmp
  let vm
  let run = false;

  function testRunFn() {
    return run = true;
  }

  beforeEach(() => {
    cmp = shallow(CatalogCard)
    vm = mount (CatalogCard,{
      propsData: {
        id : 1,
        title : "Title",
        status : "draft",
        onClick: testRunFn
      }
    })
  })

  it('should show something', () => {
    expect(cmp).toBeTruthy()
  })

  it('run function on click', () => {
    vm.trigger('click')
    expect(run).toBe(false)
  })

  it('shows card title', () => {
    expect(vm.find('.title-name')[0].text()).toContain('Title')
  })

  it('shows status', () => {
    expect(vm.find('.status')[0].text()).toEqual('draft')
  })

})