import Vue from 'vue';
import { shallow, mount } from 'avoriaz';
import ProductFilter from '../components/ProductFilter.vue';

describe('Components ', () => {
    let cmp
    beforeEach(() => {
      cmp = mount(ProductFilter)
    })
  
    it('should show product filter Component', () => {
      expect(cmp).toBeTruthy()
    })

})