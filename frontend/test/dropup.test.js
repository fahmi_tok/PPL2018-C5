// Vue test utils: https://github.com/eddyerburgh/avoriaz
import Vue from 'vue';
import { shallow, mount } from 'avoriaz';
import Dropup from '../components/Dropup';
import Button from '../components/Button';

describe('Dropup ', () => {
  let cmp
  beforeEach(() => {
    function setProductsfn() {
      return;
    }
    cmp = mount(Dropup, {
      propsData: {
        setProducts: setProductsfn
      }
    })
  })

  it('should show dropup Component', () => {
    expect(cmp).toBeTruthy()
  })

  it('should hide dropdown menu before click', () => {
    expect(cmp.find('.show').length).toEqual(0)
  })

  it('should show dropdown menu on click', () => {
    cmp.trigger('click')
    expect(cmp.find('.show')).toBeTruthy()
  })
})