// Vue test utils: https://github.com/eddyerburgh/avoriaz
import Vue from 'vue';
import { shallow } from 'avoriaz';
import { mount } from 'avoriaz';
import Container from '../components/Container';

describe('Components ', () => {
  let cmp
  let vm

  beforeEach(() => {
    cmp = shallow(Container)
    vm = mount (Container,{
      propsData: {
        id : 1,
        thumbnail : "https://4vector.com/i/free-vector-rubik-s-cube-random-clip-art_106251_Rubiks_Cube_Random_clip_art_medium.png",
        title : "Title",
        price : "140,000",
        isDraft : "1"
      }
    })
  })

  it('should show something', () => {
    // To render DOM example: cmp.html()
    expect(cmp).toBeTruthy()
  })

  it('shows image', () => {
    expect(vm.contains('img')).toBe(true)
  })

  it('shows card title', () => {
    expect(vm.find('.card-title')[0].text()).toEqual('Title')
  })

  it('shows price', () => {
    expect(vm.find('.price')[0].text()).toEqual('Rp. 140,000 /bulan')
  })

  it('show tags', () => {
    expect(vm.find('.status')[0].text()).toEqual('draft')
  })

})
