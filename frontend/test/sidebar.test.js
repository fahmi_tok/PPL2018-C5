import Vue from 'vue';
import { mount, shallow } from 'avoriaz';
import Sidebar from '../components/Sidebar';

describe('AppLogo ', () => {
  let cmp
  let vm

  cmp = mount(Sidebar, {
    data: function() {
      return {
        routes: [
          {
            page: 'Produk',
            href: 'toggleProduk',
            routes: [
              {
                page: 'Tambah',
                href: '#'
              },
              {
                page: 'Browse',
                href: '#'
              }
            ]
          }
        ]
      }
    }
  })

  vm = cmp.html()

  it('should show something', () => {
    expect(cmp).toBeTruthy()
  })

  it('should render the sidebar element as string', () => {
    expect(vm).toBeTruthy()
  })

  it('should define the data', () => {
    expect(cmp.data().routes).toBeDefined()
  })

  it('should have "page" attribute', () => {
    expect(cmp.data().routes[0].page).toBe('Produk')
  })

  it('should have child element', () => {
    expect(cmp.data().routes[0].routes).toHaveLength(2)
  })

  it('should render ul attribute', () => {
    expect(cmp.contains('ul')).toBe(true)
  })

  it('should render li attribute', () => {
    expect(cmp.contains('li')).toBe(true)
  })

  it('should not render image attribute', () => {
    expect(cmp.contains('img')).toBe(false)
  })

  it('should render li attribute', () => {
    expect(cmp.contains('li')).toBe(true)
  })

})
