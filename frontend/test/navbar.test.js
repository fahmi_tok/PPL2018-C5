// Vue test utils: https://github.com/eddyerburgh/avoriaz
import Vue from 'vue';
import { shallow, mount } from 'avoriaz';
import Navbar from '../components/Navbar';

describe('Components ', () => {
  let cmp
  beforeEach(() => {
    cmp = shallow(Navbar)
  })

  it('should show something', () => {
    expect(cmp).toBeTruthy()
  })

  it('should show Navbar Component', () => {
    expect(cmp).toBeTruthy()
  })

  it('renders a nav', () => {
    expect(cmp.find('nav').length).toBe(1)
  })

  it('renders an a', () => {
    expect(cmp.find('a').length).toBe(1)
  })

  it('renders an img', () => {
    expect(cmp.find('img').length).toBe(1)
  })
})