import Vue from 'vue';
import { shallow } from 'avoriaz';
import { mount } from 'avoriaz';
import ProductDetail from '../components/ProductDetail';

describe('Components ', () => {
  let cmp
  let vm

  beforeEach(() => {
    cmp = shallow(ProductDetail)
    vm = mount (ProductDetail,{
      propsData: {
        id: 1,
        thumbnail: "https://cdn.mapan.id/images/products/2018/3/31/1/1/819abb1.jpeg",
        title: "Tas Cantik",
        price: "50.000",
        description: "Tampil cantik tak harus mahal, lengkapi penampilan dengan tas selempang berdesain klasik dan elegan. Tas berbahan kulit sintetis warna abu-abu ini memiliki kantong penyimpanan yang cukup untuk menampung kebutuhan penting Anda.",
        specs: "Bahan: Kulit Sintetis PVC (Sponge) | Warna: Abu-abu | Ukuran: 34 x 12.5 x 27 cm"
      }
    })
  })

  it('should show something', () => {
    expect(cmp).toBeTruthy()
  })

  it('shows image', () => {
    expect(vm.contains('img')).toBe(true)
  })

  it('shows card title', () => {
    expect(vm.find('.title-name')[0].text()).toEqual('Tas Cantik')
  })

  it('shows price', () => {
    expect(vm.find('.price')[0].text()).toContain('Rp. 50.000 /bulan')
  })

  it('shows description', () => {
    expect(vm.find('.desc')[0].text()).not.toEqual('')
  })

  it('shows specs', () => {
    expect(vm.find('.specs')[0].text()).not.toEqual('')
  })

})
