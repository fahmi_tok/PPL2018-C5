package googledrive

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"

	drive "google.golang.org/api/drive/v3"

	d "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/database"
	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

const BUFFER_SIZE = 10000

// GoogleDrive interface holds the contract for google drive client
// so any class will be considered as valid google drive client if it implements all the given function
type GoogleDrive interface {
	// GetImagesByID holds the logic for fetching image(s) from googledrive
	// given a product SKU and return the b64 of the image(s) of related product
	RetrieveAllImage(context.Context, m.Product)
}

type googleDrive struct {
	database d.Database
	worker   chan job
}

type GDriveOpt struct {
	Database     d.Database
	Oauth2Token  string
	ClientSecret string
	GScriptID    string
	NumWorker    int
}

type job struct {
	Product m.Product
}

type GDriveResponse struct {
	Name     string `json:"name"`
	MimeType string `json:"mimeType"`
	Result   string `json:"result"`
}

func New(opt GDriveOpt) (*googleDrive, error) {
	config, err := google.ConfigFromJSON([]byte(opt.ClientSecret), drive.DriveMetadataReadonlyScope)
	if err != nil {
		return &googleDrive{}, err
	}

	token := &oauth2.Token{}
	err = json.NewDecoder(strings.NewReader(opt.Oauth2Token)).Decode(token)

	if err != nil {
		return &googleDrive{}, err
	}

	service, err := drive.New(config.Client(context.Background(), token))
	if err != nil {
		return &googleDrive{}, err
	}

	workerChan := make(chan job, BUFFER_SIZE)

	for i := 0; i < opt.NumWorker; i++ {
		go worker(opt.Database, service, workerChan, opt.GScriptID)
	}

	return &googleDrive{worker: workerChan}, nil
}

func GetOauth2Token(clientSecret string) {
	config, err := google.ConfigFromJSON([]byte(clientSecret), drive.DriveMetadataReadonlyScope)
	if err != nil {
		panic(fmt.Sprintf("Unable to create config from client secret %v", err))
	}

	authUrl := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authUrl)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		panic(fmt.Sprintf("Unable to read authorization code %v", err))
	}

	token, err := config.Exchange(oauth2.NoContext, authCode)
	if err != nil {
		panic(fmt.Sprintf("Unable to retrieve token from web %v", err))
	}

	tokenByte, err := json.Marshal(token)
	if err != nil {
		panic(fmt.Sprintf("error while marshall JSON %v", err))
	}

	fmt.Println(string(tokenByte))
}

func (gd *googleDrive) RetrieveAllImage(ctx context.Context, product m.Product) {
	select {
	case <-ctx.Done():
		log.Printf("ERROR: Context error: %+v\n", ctx.Err())
	default:
	}

	gd.worker <- job{Product: product}
}

func worker(db d.Database, service *drive.Service, worker chan job, gScriptID string) {
	for {
		select {
		case task := <-worker:
			product := task.Product
			query := fmt.Sprintf("mimeType contains 'image/' and name contains '%s'", product.Code)

			// asumming 1 product will have no more than 10 image
			apiResp, err := service.Files.List().PageSize(10).
				SupportsTeamDrives(true).
				IncludeTeamDriveItems(true).
				Q(query).
				Fields("nextPageToken, files(id, name)").
				Do()
			if err != nil {
				log.Printf("ERROR: Google Drive error: %+v\n", err)
			} else if len(apiResp.Files) == 0 {
				log.Printf("ERROR: Google Drive error: no matching file found!\n")
			} else {
				for _, file := range apiResp.Files {
					fileUrl := fmt.Sprintf("https://script.google.com/macros/s/%s/exec", gScriptID)
					scriptResp, err := http.PostForm(fileUrl, url.Values{"id": {file.Id}})

					if err != nil {
						log.Printf("ERROR: Couldn't fetch image with id %s for product code %s, error %+v\n", file.Id, product.Code, err)
					} else {
						body := GDriveResponse{}
						err = json.NewDecoder(scriptResp.Body).Decode(&body)

						if err != nil {
							log.Printf("ERROR: Couldn't fetch image with id %s for product code %s, error %+v\n", file.Id, product.Code, err)
						} else {
							// create file
							extension := strings.Split(body.MimeType, "/")[1]
							completeFilename := file.Id + "." + extension
							decoded, err := base64.StdEncoding.DecodeString(body.Result)
							if err != nil {
								log.Printf("ERROR: Couldn't decode image with id %s for product code %s, error %+v\n", file.Id, product.Code, err)
							}

							outputFile, err := os.Create("static/" + completeFilename)
							if err != nil {
								log.Printf("ERROR: Couldn't crate file for image with id %s for product code %s, error %+v\n", file.Id, product.Code, err)
							}

							outputFile.Write(decoded)
							outputFile.Close()

							// add to product images
							product.Images = append(product.Images, m.Image{Filename: completeFilename})
							db.UpdateProduct(product)
						}

					}
					scriptResp.Body.Close()
				}
			}

		}
	}
}
