function doPost(e) {
  return (function(id){
    Logger.log(id);
    var file = DriveApp.getFileById(id);
    Logger.log(file.getName());
    return ContentService
          .createTextOutput(JSON.stringify({
            result: Utilities.base64Encode(file.getBlob().getBytes()),
            name: file.getName(),
            mimeType: file.getBlob().getContentType()
          }))
          .setMimeType(ContentService.MimeType.JSON);
  })(e.parameters.id);
}