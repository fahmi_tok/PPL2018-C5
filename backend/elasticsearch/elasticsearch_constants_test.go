package elasticsearch_test

const (

	// General response for handler to throw error
	GENERAL_ERROR_RESPONSE = `{
		"_index": "index",
		"_type": "type",
		"_id": "11",
		"_version": 1,
		"result": "error",
		"_shards": {
			"total": 2,
			"successful": 0,
			"failed": 2
		},
		"_seq_no": 0,
		"_primary_term": 1
	}`

	// Instantiation Response && Error
	ELASTIC_CONNECT_RESPONSE = `{
								  "name" : "-dc0S7v",
								  "cluster_name" : "elasticsearch",
								  "cluster_uuid" : "56j5GSGoR4eEEN8atL7FYw",
								  "version" : {
								    "number" : "6.2.2",
								    "build_hash" : "10b1edd",
								    "build_date" : "2018-02-16T19:01:30.685723Z",
								    "build_snapshot" : false,
								    "lucene_version" : "7.2.1",
								    "minimum_wire_compatibility_version" : "5.6.0",
								    "minimum_index_compatibility_version" : "5.0.0"
								  },
								  "tagline" : "You Know, for Search"
								}`
	INSTANTIATE_ERROR_INVOKER = `INSTANTIATION_ERROR`

	//Index product related
	INDEX_PRODUCT_SUCCESS_RESPONSE = `{
								    "_index": "index",
								    "_type": "type",
								    "_id": "11",
								    "_version": 1,
								    "result": "created",
								    "_shards": {
								        "total": 2,
								        "successful": 1,
								        "failed": 0
								    },
								    "_seq_no": 0,
								    "_primary_term": 1
								}`

	// Retrieve product related
	RETRIEVE_ALL_PRODUCT_NOT_FOUND_RESPONSE = `{
										  "took" : 5,
										  "timed_out" : false,
										  "_shards" : {
										    "total" : 5,
										    "successful" : 5,
										    "skipped" : 0,
										    "failed" : 0
										  },
										  "hits" : {
										    "total" : 0,
										    "max_score" : null,
										    "hits" : [ ]
										  }
										}`

	RETRIEVE_ALL_PRODUCT_FOUND_RESPONSE = `{
									  "took" : 2,
									  "count" : 5,
									  "timed_out" : false,
									  "_shards" : {
									    "total" : 5,
									    "successful" : 5,
									    "skipped" : 0,
									    "failed" : 0
									  },
									  "hits" : {
									    "total" : 1,
									    "max_score" : 1.0,
									    "hits" : [
									      {
									        "_index" : "index",
									        "_type" : "type",
									        "_id" : "101",
									        "_score" : 1.0,
									        "_source" : {}
									      }
									    ]
									  }
									}`

	// Update product related
	UPDATE_PRODUCT_SUCCESS_RESPONSE = `{
								    "_index": "index",
								    "_type": "type",
								    "_id": "102",
								    "_version": 1,
								    "result": "updated",
								    "_shards": {
								        "total": 2,
								        "successful": 1,
								        "failed": 0
								    },
								    "_seq_no": 2,
								    "_primary_term": 1
								}`

	//Delete product response
	DELETE_PRODUCT_SUCCESS_RESPONSE = `{

									"_index": "index",
									"_type": "type",
									"_id": "101",
									"_version": 1,
									"result": "deleted",
									"_shards": {
										"total": 2,
										"failed": 0,
										"successful": 2
									},
									"_seq_no": 0,
									"_primary_term": 1
								}`

	DELETE_PRODUCT_NOT_FOUND_RESPONSE = `{

									"_index": "index",
									"_type": "type",
									"_id": "101",
									"_version": 1,
									"result": "not_found",
									"_shards": {
										"total": 2,
										"failed": 2,
										"successful": 0
									},
									"_seq_no": 0,
									"_primary_term": 1
								}`
)
