package elasticsearch

import (
	"context"
	"reflect"
	"strconv"
	"strings"

	"github.com/olivere/elastic"

	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

// elasticClient holds the structure of Elastic Client
type elasticClient struct {
	client    *elastic.Client
	indexName string
	typeName  string
}

// ElasticClient holds the contract for Elastic Client interface
type ElasticClient interface {
	// IndexProduct  should holds implementation for storing a product to elastic search.
	// expected behaviour is to update if exist and create if not exist
	// It should return ResponseStatus (whether is `created` or `updated`) an error.
	// separate contract between index and update for clearer purpose
	IndexProduct(context.Context, m.Product) (ResponseStatus, error)
	// RetrieveAllProduct should holds implementation for retrieving products from elastic search.
	RetrieveAllProduct(context.Context, m.Query) (m.Products, error)
	// IndexProduct should holds implementation for updating a product to elastic search.
	// expected behaviour is to update if exist and create if not exist
	// It should return ResponseStatus (whether is `created` or `updated`) an error.
	// separate contract between index and update for clearer purpose
	UpdateProduct(context.Context, m.Product) (ResponseStatus, error)
	// DeleteProduct should holds implementation for removing product from ElasticSearch
	DeleteProduct(context.Context, m.Product) (ResponseStatus, error)
}

// ElasticOpt holds all necessary option for ElasticSearch
type ElasticOpt struct {
	IndexName string
	TypeName  string
	HostURL   string
}

// Response status holds enumeration of possible responses from ElasticSearch
type ResponseStatus = string

const (
	ELASTIC_INDEX_DOC_RESPONSE     ResponseStatus = "created"
	ELASTIC_UPDATE_DOC_RESPONSE    ResponseStatus = "updated"
	ELASTIC_DELETE_DOC_RESPONSE    ResponseStatus = "deleted"
	ELASTIC_NOT_FOUND_DOC_RESPONSE ResponseStatus = "not_found"
)

// New is a function for creating client based on HostUrl, Index and Type Name
func New(opt ElasticOpt) (*elasticClient, error) {
	client, err := elastic.NewClient(elastic.SetURL(opt.HostURL), elastic.SetSniff(false))

	if err != nil {
		return nil, err
	}

	return &elasticClient{client: client, indexName: opt.IndexName, typeName: opt.TypeName}, nil
}

// IndexProduct is a function to insert new product into ElasticSearch
func (ec *elasticClient) IndexProduct(ctx context.Context, product m.Product) (ResponseStatus, error) {
	select {
	case <-ctx.Done():
		return "", ctx.Err()
	default:
	}

	res, err := ec.client.Index().Index(
		ec.indexName,
	).Type(
		ec.typeName,
	).Id(
		strconv.FormatUint(uint64(product.ID), 10),
	).BodyJson(
		product,
	).Do(
		ctx,
	)

	if err != nil {
		return "", err
	}

	return res.Result, nil
}

// Retrieve is a function to retrieve products from ElasticSearch that match the input query
func (ec *elasticClient) RetrieveAllProduct(ctx context.Context, input m.Query) (m.Products, error) {
	select {
	case <-ctx.Done():
		return m.Products{}, ctx.Err()
	default:
	}

	query := elastic.NewBoolQuery()
	if strings.Contains(input.Keyword, " ") {
		words := strings.Split(input.Keyword, " ")
		for _, word := range words {
			query = query.Should(elastic.NewWildcardQuery("name", "*"+word+"*"))
		}
	} else {
		query = query.Should(elastic.NewWildcardQuery("name", "*"+input.Keyword+"*"))

	}
	query = query.Should(elastic.NewWildcardQuery("brand", "*"+input.Keyword+"*"))
	query = query.Should(elastic.NewWildcardQuery("description", "*"+input.Keyword+"*"))
	query = query.Should(elastic.NewWildcardQuery("category.name", "*"+input.Keyword+"*"))
	for _, filter := range input.Filter {
		if filter == "active" {
			query = query.Should(elastic.NewTermQuery("is_active", 1))
		} else if filter == "deleted" {
			query = query.Should(elastic.NewTermQuery("is_deleted", 1))
		} else if filter == "draft" {
			query = query.Should(elastic.NewTermQuery("is_draft", 1))
		}
	}

	offset := input.Pagination.Offset
	limit := input.Pagination.Limit

	res, err := ec.client.Search(
		ec.indexName,
	).Type(
		ec.typeName,
	).Query(
		query,
	).Size(
		limit,
	).From(
		offset,
	).Do(
		ctx,
	)

	if err != nil {
		return m.Products{}, err
	}

	cnt, _ := ec.client.Count(
		ec.indexName,
	).Type(
		ec.typeName,
	).Query(
		query,
	).Do(
		ctx,
	)

	return m.Products{Products: convertToProduct(res), HitCount: int(cnt)}, nil
}

// UpdateProduct is a function to update existing product from ElasticSearch
func (ec *elasticClient) UpdateProduct(ctx context.Context, product m.Product) (ResponseStatus, error) {
	return ec.IndexProduct(ctx, product)
}

// DeleteProduct is implementation to delete product from ElasticSearch
func (ec *elasticClient) DeleteProduct(ctx context.Context, product m.Product) (ResponseStatus, error) {
	select {
	case <-ctx.Done():
		return "", ctx.Err()
	default:
	}

	res, err := ec.client.Delete().Index(
		ec.indexName,
	).Type(
		ec.typeName,
	).Id(
		strconv.FormatUint(uint64(product.ID), 10),
	).Do(
		ctx,
	)

	if err != nil {
		return "", err
	}

	return res.Result, nil
}

// convertToProduct is a helper function to convert elastic SearchResult instance into Product
func convertToProduct(res *elastic.SearchResult) []m.Product {
	var lst []m.Product
	for _, item := range res.Each(reflect.TypeOf(m.Product{})) {
		lst = append(lst, item.(m.Product))
	}

	return lst
}
