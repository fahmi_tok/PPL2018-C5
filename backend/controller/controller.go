package controller

import (
	"context"
	"log"

	d "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/database"
	e "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/elasticsearch"
	g "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/googledrive"
	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

const BUFFER_SIZE = 10000

// controller holds the structure of Contoller
type controller struct {
	googleDrive g.GoogleDrive
	database    d.Database
	elastic     e.ElasticClient
	worker      chan job
}

type job struct {
	product m.Product
	ctx     context.Context
}

// Controller holds the contract of Contoller Interface
type Controller interface {
	// BulkCreateProduct should holds implementation for creating multiple product and store it in db and elasticsearch
	// It doesn't return anything since it will be executed in background
	// Any error will be delivered via Logging
	BulkCreateProduct(context.Context, []m.Product, uint) uint
	// CreateProduct should holds implementation for creating product and store it in db and elasticsearch
	// It returns Product object and error, if any
	CreateProduct(context.Context, m.Product) (m.Product, error)
	// RetrieveProductByID should holds implementation for retrieving single product with the given id
	// It returns Product object and error, if any
	RetrieveProductByID(context.Context, uint) (m.Product, error)
	// RetrieveAllProduct should holds implementation for retrieving products with the given query
	// It returns Product objects and error, if any
	RetrieveAllProduct(context.Context, m.Query) (m.Products, error)
	// UpdateProduct should holds implementation for updating corresponding product
	// It returns the corresponding Product object and error, if any
	UpdateProduct(context.Context, m.Product) (m.Product, error)
	// DeleteProduct should holds implementation for deleting corresponding product
	// It returns the corresponding Product object and error, if any
	DeleteProduct(context.Context, uint) (m.Product, error)

	// CreateCatalog should holds implementation for creating catalog and store it in db
	// It returns Catalog object and error, if any
	CreateCatalog(context.Context, m.Catalog) (m.Catalog, error)
	// RetrieveCatalogByID should holds implementation for retrieving single Catalog with the given id
	// It returns Catalog object and error, if any
	RetrieveCatalogByID(context.Context, uint) (m.Catalog, error)
	// RetrieveCatalogByUploadID should holds implementation for retrieving single Catalog with the given upload id
	// It returns Catalog object and error, if any
	RetrieveProductByUploadID(context.Context, uint, m.Pagination) (m.Products, error)
	// RetrieveAllCatalog should holds implementation for retrieving catalog by pagination
	// It returns corresponding catalogs and error, if any
	RetrieveAllCatalog(context.Context, m.Pagination) ([]m.Catalog, error)
	// UpdateCatalog should hold implementation for uodating catalog
	// It returns updated catalog and error, if any
	UpdateCatalog(context.Context, m.Catalog) (m.Catalog, error)
	// DeleteCatalog should holds implementation for for deleting catalog
	// It gives response as json string with the default structure
	DeleteCatalog(context.Context, uint) (m.Catalog, error)
}

type ControllerOpt struct {
	GoogleDrive   g.GoogleDrive
	Database      d.Database
	ElasticSearch e.ElasticClient
	NumWorker     int
}

func New(opt ControllerOpt) Controller {
	workerChannel := make(chan job, BUFFER_SIZE)

	for i := 0; i < opt.NumWorker; i++ {
		go worker(opt.Database, opt.ElasticSearch, opt.GoogleDrive, workerChannel)
	}

	return &controller{
		googleDrive: opt.GoogleDrive,
		database:    opt.Database,
		elastic:     opt.ElasticSearch,
		worker:      workerChannel,
	}
}

func worker(db d.Database, es e.ElasticClient, gd g.GoogleDrive, workerChannel chan job) {
	for {
		select {
		case job := <-workerChannel:
			ctx := job.ctx
			product := job.product
			product, err := db.CreateProduct(product)
			if err != nil {
				log.Printf("ERROR: Database error on product with SKU %s, error %+v\n", product.Code, err)
			} else {
				_, err = es.IndexProduct(ctx, product)
				if err != nil {
					log.Printf("ERROR: ElasticSearch error on product with SKU %s, error %+v\n", product.Code, err)
				} else {
					gd.RetrieveAllImage(ctx, product)
				}
			}
		}
	}
}

// BulkCreateProduct is a function for creating multiple product and store it in db and elasticsearch
func (c *controller) BulkCreateProduct(ctx context.Context, products []m.Product, uploadid uint) uint {
	select {
	case <-ctx.Done():
		log.Printf("ERROR: Context error: %+v\n", ctx.Err())
	default:
	}

	for _, product := range products {
		c.worker <- job{product: product, ctx: ctx}
	}
	return uploadid
}

// CreateProduct is a function for creating product and store it in db and elasticsearch
func (c *controller) CreateProduct(ctx context.Context, product m.Product) (m.Product, error) {
	select {
	case <-ctx.Done():
		return m.Product{}, ctx.Err()
	default:
	}

	product, err := c.database.CreateProduct(product)
	if err != nil {
		return m.Product{}, err
	}

	_, err = c.elastic.IndexProduct(ctx, product)
	if err != nil {
		return m.Product{}, err
	}

	return product, nil
}

// RetrieveProductByID is a function for retrieving product with the given id
func (c *controller) RetrieveProductByID(ctx context.Context, id uint) (m.Product, error) {
	select {
	case <-ctx.Done():
		return m.Product{}, ctx.Err()
	default:
	}

	product, err := c.database.RetrieveProductByID(id)
	if err != nil {
		return m.Product{}, err
	}

	return product, nil
}

// RetrieveProductByUploadID is a function for retrieving product with the given upload id
func (c *controller) RetrieveProductByUploadID(ctx context.Context, uploadid uint, page m.Pagination) (m.Products, error) {
	select {
	case <-ctx.Done():
		return m.Products{}, ctx.Err()
	default:
	}

	products, err := c.database.RetrieveProductByUploadID(uploadid, page)
	if err != nil {
		return m.Products{}, err
	}

	return products, nil
}

// RetrieveAllProduct is a function for retrieving all product with the given query
func (c *controller) RetrieveAllProduct(ctx context.Context, query m.Query) (m.Products, error) {
	select {
	case <-ctx.Done():
		return m.Products{}, ctx.Err()
	default:
	}

	var products m.Products
	var err error

	if query.Keyword != "" || len(query.Filter) != 0 {
		products, err = c.elastic.RetrieveAllProduct(ctx, query)
		if err != nil {
			return m.Products{}, err
		}
	} else {
		page := query.Pagination
		products, err = c.database.RetrieveAllProduct(page)
		if err != nil {
			return m.Products{}, err
		}
	}

	return products, nil
}

// UpdateProduct is a function for uppdating corresponding product
func (c *controller) UpdateProduct(ctx context.Context, product m.Product) (m.Product, error) {
	select {
	case <-ctx.Done():
		return m.Product{}, ctx.Err()
	default:
	}

	product, err := c.database.UpdateProduct(product)
	if err != nil {
		return m.Product{}, err
	}

	_, err = c.elastic.UpdateProduct(ctx, product)
	if err != nil {
		return m.Product{}, err
	}

	return product, nil
}

// DeleteProduct is a function for deleting corresponding product
func (c *controller) DeleteProduct(ctx context.Context, id uint) (m.Product, error) {
	select {
	case <-ctx.Done():
		return m.Product{}, ctx.Err()
	default:
	}

	product, err := c.database.DeleteProduct(id)
	if err != nil {
		return m.Product{}, err
	}

	_, err = c.elastic.DeleteProduct(ctx, product)
	if err != nil {
		return m.Product{}, err
	}

	return product, nil
}

// CreateCatalog is a function for creating corresponding catalog
func (c *controller) CreateCatalog(ctx context.Context, catalog m.Catalog) (m.Catalog, error) {
	select {
	case <-ctx.Done():
		return m.Catalog{}, ctx.Err()
	default:
	}

	catalog, err := c.database.CreateCatalog(catalog)
	if err != nil {
		return m.Catalog{}, err
	}

	return catalog, nil
}

// RetrieveCatalogByID is a function for retrieving Catalog with the given id
func (c *controller) RetrieveCatalogByID(ctx context.Context, id uint) (m.Catalog, error) {
	select {
	case <-ctx.Done():
		return m.Catalog{}, ctx.Err()
	default:
	}

	catalog, err := c.database.RetrieveCatalogByID(id)
	if err != nil {
		return m.Catalog{}, err
	}

	return catalog, nil
}

// RetrieveAllCatalog is a function for getting catalogs within specified pagination range
func (c *controller) RetrieveAllCatalog(ctx context.Context, pagination m.Pagination) ([]m.Catalog, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	catalogs, err := c.database.RetrieveAllCatalog(pagination)
	if err != nil {
		return nil, err
	}

	return catalogs, nil
}

// UpdateCatalog is a function for updating given catalog
func (c *controller) UpdateCatalog(ctx context.Context, catalog m.Catalog) (m.Catalog, error) {
	select {
	case <-ctx.Done():
		return m.Catalog{}, ctx.Err()
	default:
	}

	catalog, err := c.database.UpdateCatalog(catalog)
	if err != nil {
		return m.Catalog{}, err
	}

	return catalog, nil
}

// DeleteCatalog is a function for deleting corresponding catalog
func (c *controller) DeleteCatalog(ctx context.Context, id uint) (m.Catalog, error) {
	select {
	case <-ctx.Done():
		return m.Catalog{}, ctx.Err()
	default:
	}

	catalog, err := c.database.DeleteCatalog(id)
	if err != nil {
		return m.Catalog{}, err
	}

	return catalog, nil
}
