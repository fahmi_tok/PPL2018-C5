package model

import (
	"time"
)

// Product model
// Product holds minimal property as previously written in PT Mapan CSV template
// Product will have one category (category is stored in different table, product refer  category foreign key)
// Images propery belongs to product, we separate the table from product since product will have multiple images
// Price propery belongs to product, we separate the table from product since price may change frequently, so update query will be lighter
type Product struct {
	ID                uint       `json:"id" gorm:"primary_key"`
	UploadID          uint       `json:"upload_id"`
	CreatedAt         time.Time  `json:"created_at"`
	UpdatedAt         time.Time  `json:"updated_at"`
	DeletedAt         *time.Time `json:"deleted_at"`
	IsDeleted         string     `json:"is_deleted" gorm:"default:false"`
	IsActive          string     `json:"is_active" gorm:"default:false"`
	IsDraft           string     `json:"is_draft" gorm:"default:true"`
	Code              string     `json:"code" gorm:"UNIQUE"`
	Name              string     `json:"name"`
	Description       string     `json:"description"`
	Specs             string     `json:"specs"`
	Weight            string     `json:"weight"`
	Length            string     `json:"length"`
	Width             string     `json:"width"`
	Height            string     `json:"height"`
	Brand             string     `json:"brand"`
	Status            string     `json:"status"`
	Stock             string     `json:"stock"`
	AlertLevel        string     `json:"alert_level"`
	StopLevel         string     `json:"stop_level"`
	NeedMemberAddress string     `json:"need_member_address"`

	Category   Category `json:"category"`
	CategoryID uint     `json:"category_id"`

	Price Price `json:"price"`

	Images []Image `json:"images"`
}

type Products struct {
	Products []Product
	HitCount int
}

// Category model
// Category will be stored independence from product since multiple product may refer to same category
type Category struct {
	ID        uint       `json:"id" gorm:"primary_key"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
	Name      string     `json:"name" gorm:"UNIQUE"`
	Products  []Product
}

// Image model
// Image will be stored independence from product since product may have multiple images
type Image struct {
	ID        uint       `json:"id" gorm:"primary_key"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
	Filename  string     `json:"filename"`
	ProductID uint       `json:"product_id"`
}

// Price model
// Price will be stored independence from product since price may have frequent update
// So separating price from product will produce a lighter update mechanism
type Price struct {
	ID         uint       `json:"id" gorm:"primary_key"`
	CreatedAt  time.Time  `json:"created_at"`
	UpdatedAt  time.Time  `json:"updated_at"`
	DeletedAt  *time.Time `json:"deleted_at"`
	PriceZoneA string     `json:"price_zone_A"`
	PriceZoneB string     `json:"price_zone_B"`
	BonusZoneA string     `json:"bonus_zone_A"`
	BonusZoneB string     `json:"bonus_zone_B"`
	ProductID  uint       `json:"product_id"`
}

// Catalog model
// Catalog hold minimal property for catalog management
// Catalog has and belongs to many products, use `catalog_products` as join table
type Catalog struct {
	ID        uint       `json:"id" gorm:"primary_key"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
	Name      string     `json:"name" gorm:"UNIQUE"`
	IsDeleted string     `json:"is_deleted" gorm:"default:false"`
	IsActive  string     `json:"is_active" gorm:"default:false"`
	IsDraft   string     `json:"is_draft" gorm:"default:true"`

	StartTime time.Time `json:"start_time"`
	EndTime   time.Time `json:"end_time"`

	Products []Product `gorm:"many2many:catalog_products;"`
}

// Pagination struct
// We store this utility here because it will be a package-wide struct
// This struct holds utility for encapsulating pair of offest and limit for pagination
type Pagination struct {
	Offset int `json:"offset"`
	Limit  int `json:"limit"`
}

// Query struct
// We store this utility here because it will be a package-wide struct
// This struct holds utility for encapsulating tuple of keyword,filter and pagination for query
type Query struct {
	Keyword    string     `json:"keyword"`
	Filter     []string   `json:"filter"`
	Pagination Pagination `json:"pagination"`
}
