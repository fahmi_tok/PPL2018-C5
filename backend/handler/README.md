## API Response

### Standard Response
```json
{
  "Meta": {
    "Code": [app level error code],
    "Message": [code description],
    "Error": [app error message]
  },
  "Body": [response body]
}
```

### `GET /`
```json
{
  "Meta": {
    "Code": 210,
    "Message": "Success",
    "Error": ""
  },
  "Body": "Hello world!"
}
```

### `GET /product/:id`
```json
{
  "Meta": {
    "Code": 210,
    "Message": "Success",
    "Error": ""
  },
  "Body": {
    "id": 1,
    "CreatedAt": "2018-04-03T22:37:58+07:00",
    "UpdatedAt": "2018-04-03T22:37:58+07:00",
    "DeletedAt": null,
    "is_deleted": "0",
    "is_active": "0",
    "is_draft": "1",
    "code": "EL001",
    "name": "McBook",
    "description": "leptop kayaknya",
    "specs": "kualitas kw super",
    "weight": "6",
    "length": "7",
    "width": "8",
    "height": "9",
    "brand": "Bandam",
    "status": "0",
    "stock": "13",
    "alert_level": "10",
    "stop_level": "0",
    "need_member_address": "0",
    "Category": {
      "id": 0,
      "CreatedAt": "0001-01-01T00:00:00Z",
      "UpdatedAt": "0001-01-01T00:00:00Z",
      "DeletedAt": null,
      "name": ""
    },
    "CategoryID": 1,
    "Price": {
      "id": 0,
      "CreatedAt": "0001-01-01T00:00:00Z",
      "UpdatedAt": "0001-01-01T00:00:00Z",
      "DeletedAt": null,
      "price_zone_A": "",
      "price_zone_B": "",
      "bonus_zone_A": "",
      "bonus_zone_B": "",
      "ProductID": 0
    },
    "Images": null
  }
}
```

### `GET /product`
```json
{
  "Meta": {
    "Code": 210,
    "Message": "Success",
    "Error": ""
  },
  "Body": [
    {
      "id": 3,
      "CreatedAt": "2018-04-03T22:37:58+07:00",
      "UpdatedAt": "2018-04-03T22:37:58+07:00",
      "DeletedAt": null,
      "is_deleted": "0",
      "is_active": "0",
      "is_draft": "1",
      "code": "EL003",
      "name": "MekBuk",
      "description": "",
      "specs": "kw1 aja deh",
      "weight": "5",
      "length": "4",
      "width": "6",
      "height": "7",
      "brand": "Bandam",
      "status": "0",
      "stock": "34",
      "alert_level": "10",
      "stop_level": "0",
      "need_member_address": "0",
      "Category": {
        "id": 0,
        "CreatedAt": "0001-01-01T00:00:00Z",
        "UpdatedAt": "0001-01-01T00:00:00Z",
        "DeletedAt": null,
        "name": ""
      },
      "CategoryID": 1,
      "Price": {
        "id": 0,
        "CreatedAt": "0001-01-01T00:00:00Z",
        "UpdatedAt": "0001-01-01T00:00:00Z",
        "DeletedAt": null,
        "price_zone_A": "",
        "price_zone_B": "",
        "bonus_zone_A": "",
        "bonus_zone_B": "",
        "ProductID": 0
      },
      "Images": null
    },
    {
      "id": 4,
      "CreatedAt": "2018-04-03T22:37:58+07:00",
      "UpdatedAt": "2018-04-03T22:37:58+07:00",
      "DeletedAt": null,
      "is_deleted": "0",
      "is_active": "0",
      "is_draft": "1",
      "code": "EL004",
      "name": "Buku Emak",
      "description": "leptop kayaknya",
      "specs": "wah kalo ini kw3 sih",
      "weight": "5",
      "length": "4",
      "width": "7",
      "height": "4",
      "brand": "Ponarot",
      "status": "0",
      "stock": "23",
      "alert_level": "10",
      "stop_level": "0",
      "need_member_address": "0",
      "Category": {
        "id": 0,
        "CreatedAt": "0001-01-01T00:00:00Z",
        "UpdatedAt": "0001-01-01T00:00:00Z",
        "DeletedAt": null,
        "name": ""
      },
      "CategoryID": 1,
      "Price": {
        "id": 0,
        "CreatedAt": "0001-01-01T00:00:00Z",
        "UpdatedAt": "0001-01-01T00:00:00Z",
        "DeletedAt": null,
        "price_zone_A": "",
        "price_zone_B": "",
        "bonus_zone_A": "",
        "bonus_zone_B": "",
        "ProductID": 0
      },
      "Images": null
    }
  ]
}
```

### `GET /category`
```json
{
  "Meta": {
    "Code": 210,
    "Message": "Success",
    "Error": ""
  },
  "Body": [
    {
      "id": 1,
      "CreatedAt": "2018-04-03T22:37:58+07:00",
      "UpdatedAt": "2018-04-03T22:37:58+07:00",
      "DeletedAt": null,
      "name": "Elektronik"
    }
  ]
}
```

### `DELETE /product/:id`
```json
{
  "Meta": {
    "Code": 210,
    "Message": "Success",
    "Error": ""
  },
  "Body": {
    "id": 1,
    "CreatedAt": "2018-04-03T22:37:58+07:00",
    "UpdatedAt": "2018-04-03T22:37:58+07:00",
    "DeletedAt": null,
    "is_deleted": "0",
    "is_active": "0",
    "is_draft": "1",
    "code": "EL001",
    "name": "McBook",
    "description": "leptop kayaknya",
    "specs": "kualitas kw super",
    "weight": "6",
    "length": "7",
    "width": "8",
    "height": "9",
    "brand": "Bandam",
    "status": "0",
    "stock": "13",
    "alert_level": "10",
    "stop_level": "0",
    "need_member_address": "0",
    "Category": {
      "id": 0,
      "CreatedAt": "0001-01-01T00:00:00Z",
      "UpdatedAt": "0001-01-01T00:00:00Z",
      "DeletedAt": null,
      "name": ""
    },
    "CategoryID": 1,
    "Price": {
      "id": 0,
      "CreatedAt": "0001-01-01T00:00:00Z",
      "UpdatedAt": "0001-01-01T00:00:00Z",
      "DeletedAt": null,
      "price_zone_A": "",
      "price_zone_B": "",
      "bonus_zone_A": "",
      "bonus_zone_B": "",
      "ProductID": 0
    },
    "Images": null
  }
}
```


## Error Code
***

Application level error code, made so it doesn't overlap with HTTP error code

### 2xx Success

| Code | Description|
| --- | --- |
| 210 | Success |
| 2xx | "desc" |

### 4xx Client Errors

| Code | Description|
| --- | --- |
| 460 | Context Timeout |
| 461 | Invalid Query |
| 4xx | "desc" |

### 5xx Server Errors

| Code | Description|
| --- | --- |
| 520 | Controller Error |
| 521 | Error when retrieving file |
| 522 | Error when opening file |
| 523 | Error when parsing file |
| 5xx | "desc" |