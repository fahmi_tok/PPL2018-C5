package handler_test

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	h "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/handler"
	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

type HandlerSuite struct {
	suite.Suite
	handler h.Handler
	router  *gin.Engine
}

func performRequest(r *gin.Engine, method, url string, body io.Reader, ctx context.Context) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, url, body)
	req = req.WithContext(ctx)

	w := httptest.NewRecorder()

	r.ServeHTTP(w, req)

	return w
}

func performFileUploadRequest(r *gin.Engine, url string, ctx context.Context, paramName string, path string) *httptest.ResponseRecorder {
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}

	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	fi, err := file.Stat()
	if err != nil {
		panic(err)
	}
	file.Close()

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(paramName, fi.Name())
	if err != nil {
		panic(err)
	}
	part.Write(fileContents)

	err = writer.Close()
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest("POST", url, body)
	req = req.WithContext(ctx)
	req.Header.Add("Content-Type", writer.FormDataContentType())

	w := httptest.NewRecorder()

	r.ServeHTTP(w, req)

	return w
}

func TestHandlerSuite(t *testing.T) {
	suite.Run(t, &HandlerSuite{})
}

func (hs *HandlerSuite) SetupSuite() {
	c := &ControllerMock{}

	hs.handler = h.New(c)
}

func (hs *HandlerSuite) TestInstantiation() {
	c := &ControllerMock{}

	handler := h.New(c)

	assert.NotNil(hs.T(), handler, "Instance should not be nil!")
}

func (hs *HandlerSuite) TestPingOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.GET("/ping", hs.handler.Ping)
	w := performRequest(r, "GET", "/ping", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestPingOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/ping", hs.handler.Ping)
	w := performRequest(r, "GET", "/ping", nil, ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}

func (hs *HandlerSuite) TestRetrieveProductByIDOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.GET("/product/:id", hs.handler.RetrieveProductByID)
	w := performRequest(r, "GET", "/product/1", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestRetrieveProductByIDOnControllerError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/product/:id", hs.handler.RetrieveProductByID)
	w := performRequest(r, "GET", "/product/999", nil, ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestRetrieveProductByIDOnQueryError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/product/:id", hs.handler.RetrieveProductByID)
	w := performRequest(r, "GET", "/product/99a", nil, ctx)

	assert.Equal(hs.T(), 400, w.Code, "Code should be 400")
}

func (hs *HandlerSuite) TestRetrieveProductByIDOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/product/:id", hs.handler.RetrieveProductByID)
	w := performRequest(r, "GET", "/product/1", nil, ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}

func (hs *HandlerSuite) TestDeleteProductOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.DELETE("/product/:id", hs.handler.DeleteProduct)
	w := performRequest(r, "DELETE", "/product/1", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestDeleteProductOnControllerError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.DELETE("/product/:id", hs.handler.DeleteProduct)
	w := performRequest(r, "DELETE", "/product/999", nil, ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestDeleteProductOnQueryError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.DELETE("/product/:id", hs.handler.DeleteProduct)
	w := performRequest(r, "DELETE", "/product/99a", nil, ctx)

	assert.Equal(hs.T(), 400, w.Code, "Code should be 400")
}

func (hs *HandlerSuite) TestDeleteProductOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.DELETE("/product/:id", hs.handler.DeleteProduct)
	w := performRequest(r, "DELETE", "/product/1", nil, ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}

func (hs *HandlerSuite) TestRetrieveAllProductOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.GET("/product", hs.handler.RetrieveAllProduct)
	w := performRequest(r, "GET", "/product?keyword=truk", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestRetrieveAllProductOnQueryError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/product", hs.handler.RetrieveAllProduct)
	w := performRequest(r, "GET", "/product?keyword=tas&limit=1&offset=dua", nil, ctx)

	assert.Equal(hs.T(), 400, w.Code, "Code should be 400")
}

func (hs *HandlerSuite) TestRetrieveAllProductOnControllerError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/product", hs.handler.RetrieveAllProduct)
	w := performRequest(r, "GET", "/product?keyword=truk", nil, ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestRetrieveAllProductOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/product", hs.handler.RetrieveAllProduct)
	w := performRequest(r, "GET", "/product?keyword=tas&filter=active&filter=deleted", nil, ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}

func (hs *HandlerSuite) TestRetrieveProductByUploadIDOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.GET("/preview/:id", hs.handler.RetrieveProductByUploadID)
	w := performRequest(r, "GET", "/preview/123", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestRetrieveProductByUploadIDOnQueryError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/preview/:id", hs.handler.RetrieveProductByUploadID)
	w := performRequest(r, "GET", "/preview/asd", nil, ctx)

	assert.Equal(hs.T(), 400, w.Code, "Code should be 400")
}

func (hs *HandlerSuite) TestRetrieveProductByUploadIDOnControllerError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/preview/:id", hs.handler.RetrieveProductByUploadID)
	w := performRequest(r, "GET", "/preview/0", nil, ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestRetrieveProductByUploadIDOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/preview/:id", hs.handler.RetrieveProductByUploadID)
	w := performRequest(r, "GET", "/preview/123?limit=25", nil, ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}

func (hs *HandlerSuite) TestCreateProductByCSVOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.POST("/product", hs.handler.CreateProductByCSV)
	w := performFileUploadRequest(r, "/product", ctx, "file", "sample.csv")

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestCreateProductByCSVOnFileNotFoundError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.POST("/product", hs.handler.CreateProductByCSV)
	w := performFileUploadRequest(r, "/product", ctx, "filez", "sample.csv")

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestCreateProductByCSVOnFileParseError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.POST("/product", hs.handler.CreateProductByCSV)
	w := performFileUploadRequest(r, "/product", ctx, "file", "README.md")

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestCreateProductByCSVOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.POST("/product", hs.handler.CreateProductByCSV)
	w := performFileUploadRequest(r, "/product", ctx, "file", "sample.csv")

	assert.Equal(hs.T(), 201, w.Code, "Code should be 201")
}

func (hs *HandlerSuite) TestCreateCatalogOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.POST("/catalog", hs.handler.CreateCatalog)

	catalog := m.Catalog{
		Name:     "cat-1",
		Products: []m.Product{m.Product{ID: 1}, m.Product{ID: 5}},
	}
	catalogJSON, _ := json.Marshal(catalog)
	w := performRequest(r, "POST", "/catalog", bytes.NewReader(catalogJSON), ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestCreateCatalogOnDecodeError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.POST("/catalog", hs.handler.CreateCatalog)

	catalog := "BROKEN-DATA"
	catalogJSON, _ := json.Marshal(catalog)
	w := performRequest(r, "POST", "/catalog", bytes.NewReader(catalogJSON), ctx)

	assert.Equal(hs.T(), 400, w.Code, "Code should be 400")
}

func (hs *HandlerSuite) TestCreateCatalogOnControllerError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.POST("/catalog", hs.handler.CreateCatalog)

	catalog := m.Catalog{
		Name:     "cat-error",
		Products: []m.Product{m.Product{ID: 1}, m.Product{ID: 5}},
	}
	catalogJSON, _ := json.Marshal(catalog)
	w := performRequest(r, "POST", "/catalog", bytes.NewReader(catalogJSON), ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestCreateCatalogOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.POST("/catalog", hs.handler.CreateCatalog)

	catalog := m.Catalog{
		Name:     "cat-success",
		Products: []m.Product{m.Product{ID: 1}, m.Product{ID: 5}},
	}
	catalogJSON, _ := json.Marshal(catalog)
	w := performRequest(r, "POST", "/catalog", bytes.NewReader(catalogJSON), ctx)

	assert.Equal(hs.T(), 201, w.Code, "Code should be 201")
}

func (hs *HandlerSuite) TestRetrieveAllCatalogOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.GET("/catalog", hs.handler.RetrieveAllCatalog)
	w := performRequest(r, "GET", "/catalog", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestRetrieveAllCatalogOnControllerError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/catalog", hs.handler.RetrieveAllCatalog)
	w := performRequest(r, "GET", "/catalog?limit=1&offset=100", nil, ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestRetrieveAllCatalogOnQueryError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/catalog", hs.handler.RetrieveAllCatalog)
	w := performRequest(r, "GET", "/catalog?limit=satu&offset=dua", nil, ctx)

	assert.Equal(hs.T(), 400, w.Code, "Code should be 400")
}

func (hs *HandlerSuite) TestRetrieveAllCatalogOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/catalog", hs.handler.RetrieveAllCatalog)
	w := performRequest(r, "GET", "/catalog", nil, ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}

func (hs *HandlerSuite) TestUpdateCatalogOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.PUT("/catalog", hs.handler.UpdateCatalog)
	w := performRequest(r, "PUT", "/catalog", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestUpdateCatalogOnDecodeError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.PUT("/catalog", hs.handler.UpdateCatalog)

	catalog := "BROKEN-DATA"
	catalogJSON, _ := json.Marshal(catalog)
	w := performRequest(r, "PUT", "/catalog", bytes.NewReader(catalogJSON), ctx)

	assert.Equal(hs.T(), 400, w.Code, "Code should be 400")
}

func (hs *HandlerSuite) TestUpdateCatalogOnControllerError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.PUT("/catalog", hs.handler.UpdateCatalog)

	catalog := m.Catalog{
		Name:     "Err",
		Products: []m.Product{m.Product{ID: 1}, m.Product{ID: 1}},
	}
	catalogJSON, _ := json.Marshal(catalog)
	w := performRequest(r, "PUT", "/catalog", bytes.NewReader(catalogJSON), ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestUpdateCatalogOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.PUT("/catalog", hs.handler.UpdateCatalog)

	catalog := m.Catalog{
		Name:     "update-success",
		Products: []m.Product{m.Product{ID: 1}, m.Product{ID: 1}},
	}
	catalogJSON, _ := json.Marshal(catalog)
	w := performRequest(r, "PUT", "/catalog", bytes.NewReader(catalogJSON), ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}

func (hs *HandlerSuite) TestDeleteCatalogOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.DELETE("/catalog/:id", hs.handler.DeleteCatalog)
	w := performRequest(r, "DELETE", "/catalog/1", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestDeleteCatalogOnControllerError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.DELETE("/catalog/:id", hs.handler.DeleteCatalog)
	w := performRequest(r, "DELETE", "/catalog/999", nil, ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestDeleteCatalogOnQueryError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.DELETE("/catalog/:id", hs.handler.DeleteCatalog)
	w := performRequest(r, "DELETE", "/catalog/99a", nil, ctx)

	assert.Equal(hs.T(), 400, w.Code, "Code should be 400")
}

func (hs *HandlerSuite) TestDeleteCatalogOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.DELETE("/catalog/:id", hs.handler.DeleteCatalog)
	w := performRequest(r, "DELETE", "/catalog/1", nil, ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}

func (hs *HandlerSuite) TestRetrieveCatalogByIDOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.GET("/catalog/:id", hs.handler.RetrieveCatalogByID)
	w := performRequest(r, "GET", "/catalog/1", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestRetrieveCatalogByIDOnControllerError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/catalog/:id", hs.handler.RetrieveCatalogByID)
	w := performRequest(r, "GET", "/catalog/999", nil, ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestRetrieveCatalogByIDOnQueryError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/catalog/:id", hs.handler.RetrieveCatalogByID)
	w := performRequest(r, "GET", "/catalog/99a", nil, ctx)

	assert.Equal(hs.T(), 400, w.Code, "Code should be 400")
}

func (hs *HandlerSuite) TestRetrieveCatalogByIDOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/catalog/:id", hs.handler.RetrieveCatalogByID)
	w := performRequest(r, "GET", "/catalog/1", nil, ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}

func (hs *HandlerSuite) TestGetImageOnContextTimeout() {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Nanosecond)
	defer cancel()

	r := gin.Default()
	r.GET("/image/:filename", hs.handler.GetImage)
	w := performRequest(r, "GET", "/image/dummy.txt", nil, ctx)

	assert.Equal(hs.T(), 408, w.Code, "Code should be 408")
}

func (hs *HandlerSuite) TestGetImageOnFileNotFoundError() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/image/:filename", hs.handler.GetImage)
	w := performRequest(r, "GET", "/image/SKU-1234.jpg", nil, ctx)

	assert.Equal(hs.T(), 500, w.Code, "Code should be 500")
}

func (hs *HandlerSuite) TestGetImageOnSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r := gin.Default()
	r.GET("/image/:filename", hs.handler.GetImage)
	w := performRequest(r, "GET", "/image/dummy.txt", nil, ctx)

	assert.Equal(hs.T(), 200, w.Code, "Code should be 200")
}
