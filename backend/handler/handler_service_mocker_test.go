package handler_test

import (
	"context"
	"errors"

	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

type ControllerMock struct{}

func (cm *ControllerMock) BulkCreateProduct(ctx context.Context, products []m.Product, uploadid uint) uint {
	return 0
}

func (cm *ControllerMock) CreateProduct(ctx context.Context, product m.Product) (m.Product, error) {
	if product.Code == "TELOR" {
		return m.Product{}, errors.New("Product with SKU TELOR not exist!")
	}
	return m.Product{}, nil
}

func (cm *ControllerMock) RetrieveAllProduct(ctx context.Context, query m.Query) (m.Products, error) {
	if query.Keyword == "tas" {
		return m.Products{}, nil
	}
	if query.Keyword == "truk" {
		return m.Products{}, errors.New("Product doesn't exist!")
	}
	return m.Products{}, nil
}

func (cm *ControllerMock) RetrieveProductByID(ctx context.Context, id uint) (m.Product, error) {
	if id > 900 {
		return m.Product{}, errors.New("Product doesn't exist!")
	}
	return m.Product{}, nil
}

func (cm *ControllerMock) RetrieveProductByUploadID(ctx context.Context, uploadid uint, page m.Pagination) (m.Products, error) {
	if uploadid < 1 {
		return m.Products{}, errors.New("UploadID doesn't exist!")
	}
	return m.Products{}, nil
}

func (cm *ControllerMock) UpdateProduct(ctx context.Context, product m.Product) (m.Product, error) {
	return product, nil
}

func (cm *ControllerMock) DeleteProduct(ctx context.Context, id uint) (m.Product, error) {
	if id > 900 {
		return m.Product{}, errors.New("Product doesn't exist!")
	}
	return m.Product{}, nil
}

func (cm *ControllerMock) CreateCatalog(ctx context.Context, catalog m.Catalog) (m.Catalog, error) {
	if catalog.Name == "cat-error" {
		return m.Catalog{}, errors.New("Couldn't create catalog!")
	}
	return catalog, nil
}

func (cm *ControllerMock) RetrieveCatalogByID(ctx context.Context, id uint) (m.Catalog, error) {
	if id > 900 {
		return m.Catalog{}, errors.New("Catalog doesn't exist!")
	}
	return m.Catalog{}, nil
}

func (cm *ControllerMock) RetrieveAllCatalog(ctx context.Context, pagination m.Pagination) ([]m.Catalog, error) {
	if pagination.Offset == 100 {
		return nil, errors.New("Couldn't retrieve catalog!")
	}

	return nil, nil
}

func (cm *ControllerMock) UpdateCatalog(ctx context.Context, catalog m.Catalog) (m.Catalog, error) {
	if catalog.Name == "Err" {
		return m.Catalog{}, errors.New("Couldn't find catalog!")
	}

	return m.Catalog{}, nil
}

func (cm *ControllerMock) DeleteCatalog(ctx context.Context, id uint) (m.Catalog, error) {
	if id > 900 {
		return m.Catalog{}, errors.New("Catalog doesn't exist!")
	}
	return m.Catalog{}, nil
}
