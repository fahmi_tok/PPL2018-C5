package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/controller"
	"gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/database"
	"gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/googledrive"
	"gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/handler"

	"gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/elasticsearch"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file, using to container ENV only.")
	}

	r := gin.Default()
	r.Use(cors.Default())

	dbUsername := os.Getenv("MYSQL_USERNAME")
	dbPassword := os.Getenv("MYSQL_PASSWORD")
	dbHost := os.Getenv("MYSQL_HOST")
	dbName := os.Getenv("MYSQL_DATABASE_NAME")

	client, err := gorm.Open("mysql",
		fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?charset=utf8&parseTime=True&loc=Local",
			dbUsername,
			dbPassword,
			dbHost,
			dbName,
		),
	)
	if err != nil {
		log.Println("Error connecting to DB")
		panic(err)
	}

	db := database.New(client)

	es, err := elasticsearch.New(elasticsearch.ElasticOpt{
		IndexName: os.Getenv("ELASTICSEARCH_INDEX"),
		TypeName:  os.Getenv("ELASTICSEARCH_TYPE"),
		HostURL:   os.Getenv("ELASTICSEARCH_HOST"),
	})
	if err != nil {
		log.Println("Error connecting to ES")
		panic(err)
	}

	gd, err := googledrive.New(googledrive.GDriveOpt{
		Database:     db,
		NumWorker:    10,
		GScriptID:    os.Getenv("GOOGLE_SCRIPT_ID"),
		ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
		Oauth2Token:  os.Getenv("GOOGLE_OAUTH_TOKEN"),
	})
	if err != nil {
		log.Println("Error connecting to Google Drive")
		panic(err)
	}

	port := os.Getenv("WEB_PORT")

	controllerOpt := controller.ControllerOpt{
		GoogleDrive:   gd,
		Database:      db,
		ElasticSearch: es,
		NumWorker:     10,
	}

	controller := controller.New(controllerOpt)
	handler := handler.New(controller)

	r.GET("/", handler.Ping)
	r.POST("/product", handler.CreateProductByCSV)
	r.GET("/product", handler.RetrieveAllProduct)
	r.GET("/product/:id", handler.RetrieveProductByID)
	r.DELETE("/product/:id", handler.DeleteProduct)

	r.POST("/catalog", handler.CreateCatalog)
	r.GET("/catalog", handler.RetrieveAllCatalog)
	r.GET("/catalog/:id", handler.RetrieveCatalogByID)
	r.PUT("/catalog", handler.UpdateCatalog)
	r.DELETE("/catalog/:id", handler.DeleteCatalog)

	r.GET("/preview/:id", handler.RetrieveProductByUploadID)

    r.GET("image/:filename", handler.GetImage)

	r.Run(port)
}
