package database

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"

	m "gitlab.com/PPL2018csui/Kelas-C/PPL2018-C5/backend/model"
)

// database holds the structure for Database
// its encapsulating GORM ORM
type database struct {
	client *gorm.DB
}

// Database holds the contract for DB interface
type Database interface {
	// Migrate should holds implementation for migrating any struct to database table
	Migrate(interface{})
	// CreateProduct should holds implementation for storing product in database
	CreateProduct(m.Product) (m.Product, error)
	// RetrieveAllProduct should holds implementation retrieving product within specified pagination from database.
	// pagination format will be {Offset, Limit}
	// example {1, 5} means offset 1 product and get 5 products after it
	// so you will get [product2, product3, product4, product5, product6]
	// assuming products start from product1
	RetrieveAllProduct(m.Pagination) (m.Products, error)
	// RetrieveProductByID should holds implementation for retrieving a single product corresponding the given ID
	RetrieveProductByID(uint) (m.Product, error)
	// UpdateProduct should holds implementation for updating a product corresponding the given ID
	UpdateProduct(m.Product) (m.Product, error)
	// DeleteProduct should holds implementation to delete product from DB according to given ID
	// it returns the deleted product on successful scenario and empty product + error on failed scenario
	DeleteProduct(uint) (m.Product, error)

	// CreateCategory should holds implementation for storing category in database
	CreateCategory(m.Category) (m.Category, error)
	// RetrieveAllCategory should holds implementation for retrieving categories within specified range
	// pagination format will be {Offset, Limit}
	// example {1, 5} means offset 1 category and get 5 categories after it
	// so you will get [cat2, cat3, cat4, cat5, cat6]
	// assuming categories start from cat1
	RetrieveAllCategory(m.Pagination) ([]m.Category, error)
	// RetrieveCategoryByID should holds implementation for retrieving a single category corresponding the given ID
	RetrieveCategoryByID(uint) (m.Category, error)
	// RetrieveProductByUploadID is a function for getting products from database by UploadID
	RetrieveProductByUploadID(uint, m.Pagination) (m.Products, error)
	// UpdateCategory should holds implementation for updating a category corresponding the given ID
	UpdateCategory(m.Category) (m.Category, error)
	// DeleteCategory should holds implementation to delete category from DB according to given ID
	// it returns the deleted category on successful scenario and empty category + error on failed scenario
	DeleteCategory(uint) (m.Category, error)

	// CreateCatalog should holds implementation for storing catalog in database
	CreateCatalog(m.Catalog) (m.Catalog, error)
	// RetrieveCatalogByID should holds implementation for retrieving a single catalog corresponding the given ID
	RetrieveCatalogByID(uint) (m.Catalog, error)
	// RetrieveAllCatalog shoulds holds implementation for getting all catalog within specified pagination
	RetrieveAllCatalog(m.Pagination) ([]m.Catalog, error)
	// UpdateCatalog shoulds holds implementation for updating a catalog corresponding the given ID
	UpdateCatalog(m.Catalog) (m.Catalog, error)
	// DeleteCatalog shoulds holds implementation for deleting a catalong corresponding the given ID
	DeleteCatalog(uint) (m.Catalog, error)
}

// New is used to initiate interface for the DB client
// It returns the interface for the client
func New(client *gorm.DB) Database {
	return &database{client: client}
}

// Migrate is an utility to migrate any golang struct into database table
// Encapsulating GORM Automigrate
func (d *database) Migrate(i interface{}) {
	d.client.AutoMigrate(i)
}

// CreateProduct is a function for storing product in database
func (d *database) CreateProduct(product m.Product) (m.Product, error) {

	//set created date
	product.CreatedAt = time.Now()
	product.UpdatedAt = time.Now()

	category, _ := d.CreateCategory(m.Category{Name: product.Category.Name})

	//set category as nil since its already in db
	product.Category = m.Category{}
	product.CategoryID = category.ID

	// put product to database
	if err := d.client.Where(&m.Product{Code: product.Code}).First(&m.Product{}).Error; err != nil {
		d.client.Create(&product)
	}

	// update product to latest
	d.client.Where(&m.Product{Code: product.Code}).First(&product)

	// append category
	product.Category = category

	return product, nil
}

// RetrieveAllProduct is a function for retrieving product from database within specified pagination
func (d *database) RetrieveAllProduct(page m.Pagination) (m.Products, error) {

	var products []m.Product
	var cnt int
	if err := d.client.Offset(page.Offset).Limit(page.Limit).Preload("Category").Preload("Price").Preload("Images").Find(&products).Error; err != nil {
		return m.Products{}, err
	}
	d.client.Table("products").Count(&cnt)

	return m.Products{Products: products, HitCount: cnt}, nil
}

// RetrieveProductByID is a function for retrieving product from database within specified id
func (d *database) RetrieveProductByID(id uint) (m.Product, error) {

	var product m.Product
	if err := d.client.Preload("Category").Preload("Price").Preload("Images").First(&product, id).Error; err != nil {
		return m.Product{}, err
	}

	return product, nil
}

// RetrieveProductByUploadID is a function for getting products from database by UploadID
func (d *database) RetrieveProductByUploadID(uploadid uint, page m.Pagination) (m.Products, error) {

	var products []m.Product
	var cnt int
	if err := d.client.Where(&m.Product{UploadID: uploadid}).Offset(page.Offset).Limit(page.Limit).Preload("Category").Preload("Price").Preload("Images").Find(&products).Error; err != nil {
		return m.Products{}, err
	}
	d.client.Model(&m.Product{}).Where(&m.Product{UploadID: uploadid}).Count(&cnt)

	return m.Products{Products: products, HitCount: cnt}, nil
}

// UpdateProduct is a function for updating product from database
func (d *database) UpdateProduct(product m.Product) (m.Product, error) {

	var productOld m.Product
	if err := d.client.First(&productOld, product.ID).Error; err != nil {
		return m.Product{}, err
	}

	d.client.Model(&productOld).Updates(product)

	return product, nil
}

// DeleteProduct is a function for deleting product from database
func (d *database) DeleteProduct(id uint) (m.Product, error) {

	var productOld m.Product
	if err := d.client.First(&productOld, id).Error; err != nil {
		return m.Product{}, err
	}

	d.client.Delete(&productOld)

	return productOld, nil
}

// CreateCategory is a function for storing category into database
func (d *database) CreateCategory(category m.Category) (m.Category, error) {

	if err := d.client.Where(&m.Category{Name: category.Name}).First(&m.Category{}).Error; err != nil {
		d.client.Create(&m.Category{CreatedAt: time.Now(), UpdatedAt: time.Now(), Name: category.Name})
	}

	d.client.Where(&m.Category{Name: category.Name}).First(&category)
	return category, nil
}

// RetrieveAllCategory is a function for retrieving category from database within specified pagination
func (d *database) RetrieveAllCategory(page m.Pagination) ([]m.Category, error) {

	var categories []m.Category
	if err := d.client.Offset(page.Offset).Limit(page.Limit).Find(&categories).Error; err != nil {
		return nil, err
	}

	return categories, nil
}

// RetrieveCategoryByID is a function for retrieving category from database within specified id
func (d *database) RetrieveCategoryByID(id uint) (m.Category, error) {

	var category m.Category
	if err := d.client.First(&category, id).Error; err != nil {
		return m.Category{}, err
	}

	return category, nil
}

// UpdateCategory is a function for updating category into database
func (d *database) UpdateCategory(category m.Category) (m.Category, error) {

	var oldCategory m.Category
	if err := d.client.First(&oldCategory, category.ID).Error; err != nil {
		return m.Category{}, err
	}

	d.client.Model(&oldCategory).Updates(category)

	return category, nil
}

// DeleteCategory is a function for deleting category from database
func (d *database) DeleteCategory(id uint) (m.Category, error) {

	var oldCategory m.Category
	if err := d.client.First(&oldCategory, id).Error; err != nil {
		return m.Category{}, err
	}

	d.client.Delete(&oldCategory)

	return oldCategory, nil
}

// CreateCatalog is a function for storing catalog into database
// Please make sure all the products in catalog has ID
func (d *database) CreateCatalog(catalog m.Catalog) (m.Catalog, error) {
	// check if starttime or endtime is zero value
	if (catalog.StartTime == time.Time{} || catalog.EndTime == time.Time{}) {
		return m.Catalog{}, errors.New("Incorrect datetime format in start_time or end_time!")
	}

	// check if all product has id
	var products []m.Product
	for _, product := range catalog.Products {
		if err := d.client.First(&product, product.ID).Error; err != nil {
			return m.Catalog{}, err
		}
		products = append(products, product)
	}
	catalog.Products = products

	if err := d.client.Where(&m.Catalog{Name: catalog.Name}).First(&m.Catalog{}).Error; err != nil {
		catalog.CreatedAt = time.Now()
		catalog.UpdatedAt = time.Now()
		d.client.Create(&catalog)
	}

	d.client.Where(&m.Catalog{Name: catalog.Name}).Preload("Products").First(&catalog)
	return catalog, nil
}

// RetrieveCatalogByID is a function for retrieving catalog from database within specified id
func (d *database) RetrieveCatalogByID(id uint) (m.Catalog, error) {

	var catalog m.Catalog
	if err := d.client.Preload("Products").Preload("Products.Category").Preload("Products.Price").Preload("Products.Images").First(&catalog, id).Error; err != nil {
		return m.Catalog{}, err
	}

	return catalog, nil
}

// RetrieveAllCatalog is a function for retrieving catalog from database within specified pagination
func (d *database) RetrieveAllCatalog(page m.Pagination) ([]m.Catalog, error) {

	var catalogs []m.Catalog
	if err := d.client.Offset(page.Offset).Limit(page.Limit).Find(&catalogs).Error; err != nil {
		return nil, err
	}

	return catalogs, nil
}

// UpdateCatalog is a function for updating catalog from database
func (d *database) UpdateCatalog(catalog m.Catalog) (m.Catalog, error) {

	var oldCatalog m.Catalog
	if err := d.client.First(&oldCatalog, catalog.ID).Error; err != nil {
		return m.Catalog{}, err
	}

	// check if all product has id
	for idx, product := range catalog.Products {
		if err := d.client.First(&product, product.ID).Error; err != nil {
			return m.Catalog{}, err
		}
		catalog.Products[idx] = product
	}
	d.client.Model(&oldCatalog).Updates(catalog)
	d.client.Model(&oldCatalog).Association("Products").Replace(catalog.Products)
	d.client.Preload("Products").First(&catalog, catalog.ID)
	return catalog, nil
}

// DeleteCatalog is a function for deleting catalog from database
func (d *database) DeleteCatalog(id uint) (m.Catalog, error) {

	var catalogOld m.Catalog
	if err := d.client.First(&catalogOld, id).Error; err != nil {
		return m.Catalog{}, err
	}

	d.client.Delete(&catalogOld)

	return catalogOld, nil
}
